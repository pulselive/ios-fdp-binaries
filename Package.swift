// swift-tools-version:5.3
import PackageDescription

let package = Package(
    name: "PLFanDataPlatform",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        .library(
            name: "PLFanDataPlatform",
            targets: [
                "PLFanDataCommon",
                "PLFanData",
                "PLFanDataRequestHelper",
                "PLMarkdown",
                "Libcmark"
            ]
        )
    ],
    targets: [
        .binaryTarget(
            name: "PLFanDataCommon",
            path: "PLFanDataCommon.xcframework"
        ),
        .binaryTarget(
            name: "PLFanData",
            path: "PLFanData.xcframework"
        ),
        .binaryTarget(
            name: "PLFanDataRequestHelper",
            path: "PLFanDataRequestHelper.xcframework"
        ),
        .binaryTarget(
            name: "PLMarkdown",
            path: "PLMarkdown.xcframework"
        ),
        .binaryTarget(
            name: "Libcmark",
            path: "Libcmark.xcframework"
        )
    ]
)
