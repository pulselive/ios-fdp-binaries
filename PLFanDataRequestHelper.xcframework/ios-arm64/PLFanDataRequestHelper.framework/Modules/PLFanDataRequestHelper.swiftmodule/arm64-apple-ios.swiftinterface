// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.7.1 (swiftlang-5.7.1.135.3 clang-1400.0.29.51)
// swift-module-flags: -target arm64-apple-ios13.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name PLFanDataRequestHelper
// swift-module-flags-ignorable: -enable-bare-slash-regex
import Foundation
import Swift
import _Concurrency
import _StringProcessing
public enum HTTPMethod : Swift.String {
  case post
  case put
  case get
  case delete
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public enum RequestParameter {
  public enum Encoding {
    case jsonEncoding
    case urlEncoding
    public static func == (a: PLFanDataRequestHelper.RequestParameter.Encoding, b: PLFanDataRequestHelper.RequestParameter.Encoding) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  case plain
  case data(Foundation.Data)
  case encodable(Swift.Encodable, encoder: Foundation.JSONEncoder = JSONEncoder())
  case parameters(parameters: [Swift.String : Any], encoding: PLFanDataRequestHelper.RequestParameter.Encoding)
}
public protocol APIRequest {
  var baseURL: Foundation.URL { get }
  var path: Swift.String { get }
  var method: PLFanDataRequestHelper.HTTPMethod { get }
  var parameters: PLFanDataRequestHelper.RequestParameter { get }
  var headers: [Swift.String : Swift.String]? { get }
}
extension PLFanDataRequestHelper.APIRequest {
  public func urlRequest() throws -> Foundation.URLRequest
  public func url() -> Foundation.URL
}
public struct BaseURLOverridingAPIRequest<T> : PLFanDataRequestHelper.APIRequest where T : PLFanDataRequestHelper.APIRequest {
  public let baseURL: Foundation.URL
  public init(baseURL: Foundation.URL, baseAPI: T)
  public var path: Swift.String {
    get
  }
  public var method: PLFanDataRequestHelper.HTTPMethod {
    get
  }
  public var parameters: PLFanDataRequestHelper.RequestParameter {
    get
  }
  public var headers: [Swift.String : Swift.String]? {
    get
  }
}
public protocol ProviderType : AnyObject {
  associatedtype T : PLFanDataRequestHelper.APIRequest
  var urlSession: Foundation.URLSession { get }
  #if compiler(>=5.3) && $Sendable
  func request(_ request: Self.T, completionHandler: @escaping (@Sendable (Swift.Result<PLFanDataRequestHelper.Response, Swift.Error>) -> Swift.Void)) -> Foundation.URLSessionDataTask?
  #endif
  #if compiler(>=5.3) && $AsyncAwait
  func request(_ request: Self.T) async throws -> (PLFanDataRequestHelper.Response)
  #endif
}
public class Provider<T> : PLFanDataRequestHelper.ProviderType where T : PLFanDataRequestHelper.APIRequest {
  final public let urlSession: Foundation.URLSession
  public init(urlSession: Foundation.URLSession)
  #if compiler(>=5.3) && $Sendable
  public func request(_ request: T, completionHandler: @escaping (@Sendable (Swift.Result<PLFanDataRequestHelper.Response, Swift.Error>) -> Swift.Void)) -> Foundation.URLSessionDataTask?
  #endif
  #if compiler(>=5.3) && $AsyncAwait
  public func request(_ request: T) async throws -> (PLFanDataRequestHelper.Response)
  #endif
  @objc deinit
}
public enum RequestHelperError : Swift.Error {
  case statusCode(PLFanDataRequestHelper.Response)
  case decodeEmptyData
  case badResponseType
}
final public class Response : Swift.Equatable {
  final public var statusCode: Swift.Int {
    get
  }
  final public let data: Foundation.Data?
  final public let request: Foundation.URLRequest
  final public let response: Foundation.HTTPURLResponse
  public init(data: Foundation.Data?, request: Foundation.URLRequest, response: Foundation.HTTPURLResponse)
  public static func == (lhs: PLFanDataRequestHelper.Response, rhs: PLFanDataRequestHelper.Response) -> Swift.Bool
  @objc deinit
}
extension PLFanDataRequestHelper.Response {
  @discardableResult
  final public func filter<R>(statusCodes: R) throws -> PLFanDataRequestHelper.Response where R : Swift.RangeExpression, R.Bound == Swift.Int
  @discardableResult
  final public func filter(statusCode: Swift.Int) throws -> PLFanDataRequestHelper.Response
  @discardableResult
  final public func filterSuccessfulStatusCodes() throws -> PLFanDataRequestHelper.Response
  @discardableResult
  final public func filterSuccessfulStatusAndRedirectCodes() throws -> PLFanDataRequestHelper.Response
  final public func map<T>(_ type: T.Type, keyPath: Swift.String? = nil, decoder: Foundation.JSONDecoder = JSONDecoder()) throws -> T where T : Swift.Decodable
}
extension PLFanDataRequestHelper.HTTPMethod : Swift.Equatable {}
extension PLFanDataRequestHelper.HTTPMethod : Swift.Hashable {}
extension PLFanDataRequestHelper.HTTPMethod : Swift.RawRepresentable {}
extension PLFanDataRequestHelper.RequestParameter.Encoding : Swift.Equatable {}
extension PLFanDataRequestHelper.RequestParameter.Encoding : Swift.Hashable {}
