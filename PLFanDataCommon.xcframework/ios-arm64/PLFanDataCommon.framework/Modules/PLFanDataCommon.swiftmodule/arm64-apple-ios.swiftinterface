// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.7.1 (swiftlang-5.7.1.135.3 clang-1400.0.29.51)
// swift-module-flags: -target arm64-apple-ios13.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name PLFanDataCommon
// swift-module-flags-ignorable: -enable-bare-slash-regex
import CoreGraphics
import Foundation
import PLMarkdown
import Swift
import UIKit
import _Concurrency
import _StringProcessing
public struct PromoWidgetDTO : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public enum WidgetConfigDTO : Swift.Decodable {
  public struct WidgetContentDTO {
  }
  public enum TemplateType : Swift.String, Swift.Decodable {
    case templateDefault
    case template2
    case template3
    case template4
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  case inlineBanner(PLFanDataCommon.WidgetConfigDTO.WidgetContentDTO)
  case popup(PLFanDataCommon.WidgetConfigDTO.WidgetContentDTO)
  case interstitial(PLFanDataCommon.WidgetConfigDTO.WidgetContentDTO)
  case pinned(PLFanDataCommon.WidgetConfigDTO.WidgetContentDTO)
  case unknown
  public init(from decoder: Swift.Decoder) throws
}
public struct WidgetDTO : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public struct WidgetJourneyDTO : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public struct PromoWidgetEntity {
}
public struct WidgetEntity {
}
public struct WidgetDataEntity {
}
public enum FDPWidgetError : Swift.Error {
  case unsupportedWidget
  case incompatibleWidget
  case languageNotFound
  public static func == (a: PLFanDataCommon.FDPWidgetError, b: PLFanDataCommon.FDPWidgetError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension UIKit.UIWindow {
  @_Concurrency.MainActor(unsafe) public static var key: UIKit.UIWindow? {
    get
  }
}
public struct FontProvider {
  public var regularFamily: Swift.String
  public var boldFamily: Swift.String
  public var italicFamily: Swift.String
  public var boldItalicFamily: Swift.String
  public var boldOverride: Swift.String?
  public var italicOverride: Swift.String?
  public var boldItalicOverride: Swift.String?
  public init(regularFamily: Swift.String, boldFamily: Swift.String? = nil, italicFamily: Swift.String? = nil, boldItalicFamily: Swift.String? = nil, boldFontOverride: Swift.String? = nil, italicFontOverride: Swift.String? = nil, boldItalicFontOverride: Swift.String? = nil)
  public static func provider(withFamily family: Swift.String, boldFontOverride: Swift.String? = nil, italicFontOverride: Swift.String? = nil, boldItalicFontOverride: Swift.String? = nil) -> PLFanDataCommon.FontProvider
  public func regularFont(size: CoreFoundation.CGFloat = 12) -> UIKit.UIFont
  public func boldFont(size: CoreFoundation.CGFloat = 12) -> UIKit.UIFont
  public func italicFont(size: CoreFoundation.CGFloat = 12) -> UIKit.UIFont
  public func boldItalicFont(size: CoreFoundation.CGFloat = 12) -> UIKit.UIFont
}
extension UIKit.UIColor {
  convenience public init?(hex: Swift.String)
  convenience public init?(rgba: Swift.String)
}
extension UIKit.UIDevice {
  @_Concurrency.MainActor(unsafe) public class var isPhone: Swift.Bool {
    get
  }
  @_Concurrency.MainActor(unsafe) public class var isPad: Swift.Bool {
    get
  }
  @_Concurrency.MainActor(unsafe) public class var deviceTypeString: Swift.String {
    get
  }
}
public protocol Mapper {
  associatedtype In
  associatedtype Out
  func map(from input: Self.In) -> Self.Out
}
public protocol ThrowableMapper {
  associatedtype In
  associatedtype Out
  func map(from input: Self.In) throws -> Self.Out
}
public protocol WidgetConfigEntityMapperProtocol {
  func map(from input: PLFanDataCommon.PromoWidgetDTO, template: PLFanDataCommon.WidgetConfigDTO.TemplateType) -> PLFanDataCommon.PromoWidgetEntity
}
public class FDPViewModelMapper : PLFanDataCommon.ThrowableMapper {
  public init(fontProvider: PLFanDataCommon.FontProvider)
  public func map(from input: PLFanDataCommon.WidgetEntity) throws -> PLFanDataCommon.WidgetViewModel
  public typealias In = PLFanDataCommon.WidgetEntity
  public typealias Out = PLFanDataCommon.WidgetViewModel
  @objc deinit
}
public class FDPViewModelTypeMapper : PLFanDataCommon.ThrowableMapper {
  public init(fontProvider: PLFanDataCommon.FontProvider)
  public func map(from input: PLFanDataCommon.WidgetDataEntity) throws -> PLFanDataCommon.WidgetTypeViewModel
  public typealias In = PLFanDataCommon.WidgetDataEntity
  public typealias Out = PLFanDataCommon.WidgetTypeViewModel
  @objc deinit
}
public class QuickConnectViewModelMapper : PLFanDataCommon.ThrowableMapper {
  public init(fontProvider: PLFanDataCommon.FontProvider)
  public func map(from input: PLFanDataCommon.WidgetDataEntity) throws -> PLFanDataCommon.WidgetViewModel
  public typealias In = PLFanDataCommon.WidgetDataEntity
  public typealias Out = PLFanDataCommon.WidgetViewModel
  @objc deinit
}
public struct FDPWidgetEntityMapper : PLFanDataCommon.ThrowableMapper {
  public init(language: Swift.String, fallback: Swift.String)
  public func map(from input: PLFanDataCommon.WidgetDTO) throws -> PLFanDataCommon.WidgetEntity
  public typealias In = PLFanDataCommon.WidgetDTO
  public typealias Out = PLFanDataCommon.WidgetEntity
}
public struct FDPWidgetConfigEntityMapper : PLFanDataCommon.ThrowableMapper {
  public init(language: Swift.String, fallback: Swift.String)
  public func map(from input: PLFanDataCommon.WidgetConfigDTO) throws -> PLFanDataCommon.WidgetDataEntity
  public typealias In = PLFanDataCommon.WidgetConfigDTO
  public typealias Out = PLFanDataCommon.WidgetDataEntity
}
public struct PromoItemViewModel : Swift.Equatable {
  public let title: PLFanDataCommon.PromoItemTextViewModel
  public let subtitle: PLFanDataCommon.PromoItemTextViewModel
  public let backgroundImage: PLFanDataCommon.PromoItemImageViewModel
  public let foregroundImage: PLFanDataCommon.PromoItemImageViewModel?
  public let imageMobile: PLFanDataCommon.PromoItemImageViewModel?
  public let imageTablet: PLFanDataCommon.PromoItemImageViewModel?
  public let imageDesktop: PLFanDataCommon.PromoItemImageViewModel?
  public let altText: Swift.String?
  public let cta: PLFanDataCommon.PromoItemTextViewModel
  public let ctaURL: Foundation.URL?
  public let template: PLFanDataCommon.WidgetConfigDTO.TemplateType
  public let position: PLFanDataCommon.PromoItemPosition?
  public let delay: PLFanDataCommon.PromoItemDelayViewModel?
  public static func == (a: PLFanDataCommon.PromoItemViewModel, b: PLFanDataCommon.PromoItemViewModel) -> Swift.Bool
}
public struct PromoItemTextViewModel : Swift.Equatable {
  public var attributedtext: Foundation.NSAttributedString?
  public var textColor: Swift.String?
  public var fontSize: CoreFoundation.CGFloat?
  public var backgroundColor: Swift.String?
  public var isHidden: Swift.Bool
  public static func == (a: PLFanDataCommon.PromoItemTextViewModel, b: PLFanDataCommon.PromoItemTextViewModel) -> Swift.Bool
}
public struct PromoItemImageViewModel : Swift.Equatable {
  public let url: Foundation.URL?
  public let isHidden: Swift.Bool
  public let height: Swift.Double
  public let width: Swift.Double
  public let aspectRatio: Swift.Double
  public func onDemandURL(width: CoreFoundation.CGFloat?, height: CoreFoundation.CGFloat?) -> Foundation.URL?
  public static func == (a: PLFanDataCommon.PromoItemImageViewModel, b: PLFanDataCommon.PromoItemImageViewModel) -> Swift.Bool
}
public enum PromoItemPosition : Swift.Equatable {
  case topLeft
  case topRight
  case bottomLeft
  case bottomRight
  public func hash(into hasher: inout Swift.Hasher)
  public static func == (a: PLFanDataCommon.PromoItemPosition, b: PLFanDataCommon.PromoItemPosition) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
}
public struct PromoItemDelayViewModel : Swift.Equatable {
  public let duration: Foundation.TimeInterval
  public let enabled: Swift.Bool
  public static func == (a: PLFanDataCommon.PromoItemDelayViewModel, b: PLFanDataCommon.PromoItemDelayViewModel) -> Swift.Bool
}
public enum WidgetPresentation : Swift.Equatable {
  case debug(PLFanDataCommon.WidgetTypeViewModel)
  case normal(PLFanDataCommon.WidgetViewModel)
  public var isInline: Swift.Bool {
    get
  }
  public var isPopup: Swift.Bool {
    get
  }
  public var isInterstitial: Swift.Bool {
    get
  }
  public var isPinned: Swift.Bool {
    get
  }
  public var promoItemModel: PLFanDataCommon.PromoItemViewModel? {
    get
  }
  public static func == (a: PLFanDataCommon.WidgetPresentation, b: PLFanDataCommon.WidgetPresentation) -> Swift.Bool
}
public struct WidgetViewModel : Swift.Equatable {
  public let widget: PLFanDataCommon.WidgetTypeViewModel
  public let infos: PLFanDataCommon.WidgetInfoViewModel
  public static func == (a: PLFanDataCommon.WidgetViewModel, b: PLFanDataCommon.WidgetViewModel) -> Swift.Bool
}
public struct WidgetInfoViewModel : Swift.Equatable {
  public let typeName: Swift.String?
  public let placementId: Swift.String
  public let variantId: Swift.String
  public let language: Swift.String
  public let journey: PLFanDataCommon.WidgetJourneyViewModel?
  public static func == (a: PLFanDataCommon.WidgetInfoViewModel, b: PLFanDataCommon.WidgetInfoViewModel) -> Swift.Bool
}
public struct WidgetJourneyViewModel : Swift.Equatable {
  public let stepId: Swift.String?
  public static func == (a: PLFanDataCommon.WidgetJourneyViewModel, b: PLFanDataCommon.WidgetJourneyViewModel) -> Swift.Bool
}
public enum WidgetTypeViewModel : Swift.Equatable {
  case inlineBanner(PLFanDataCommon.PromoItemViewModel)
  case popup(PLFanDataCommon.PromoItemViewModel)
  case interstitial(PLFanDataCommon.PromoItemViewModel)
  case pinned(PLFanDataCommon.PromoItemViewModel)
  public static func == (a: PLFanDataCommon.WidgetTypeViewModel, b: PLFanDataCommon.WidgetTypeViewModel) -> Swift.Bool
}
extension PLFanDataCommon.WidgetConfigDTO.TemplateType : Swift.Equatable {}
extension PLFanDataCommon.WidgetConfigDTO.TemplateType : Swift.Hashable {}
extension PLFanDataCommon.WidgetConfigDTO.TemplateType : Swift.RawRepresentable {}
extension PLFanDataCommon.FDPWidgetError : Swift.Equatable {}
extension PLFanDataCommon.FDPWidgetError : Swift.Hashable {}
extension PLFanDataCommon.PromoItemPosition : Swift.Hashable {}
